# coding: utf-8

__author__ = 'degibenz'

import requests as r

from functools import wraps
from django.http import HttpResponseRedirect

try:
    import simplejson as json
except ImportError:
    import json

API_URL = 'http://go-cms.ru/api/'
DEFAULT_TIMEOUT = 1

COMPLEX_METHODS = []


class _API(object):
    def __init__(self, method=None, token=None, **defaults):

        if not (token,):
            raise ValueError("Arguments token is required")

        self.token = token
        self.defaults = defaults
        self.method = method

    def _get(self, method, **kwargs):

        response = self._request(method, **kwargs)

        if not (200 <= response.status_code <= 299):
            raise Exception({
                'error_code': response.status_code,
                'error_msg': "HTTP error",
                'request_params': kwargs,
            })

        else:
            return response.json()

    def __call__(self):
        return self._get(self.method, **self.defaults)

    def _request(self, method, **kwargs):

        type_request = method.split('.')[1]

        url = API_URL + self.method

        headers = {
            "Accept": "application/json",
            "Content-Type": "application/x-www-form-urlencoded",
            "Authorization": "Token %s" % self.token
        }

        if type_request.lower() == "POST".lower():
            self.result = r.post(url, data=kwargs, headers=headers)
        else:
            self.result = r.get(url, headers=headers)

        return self.result


class API(_API):
    def get(self, method, token, **kwargs):
        return self._get(method, token, **kwargs)


def check_auth(func):
    @wraps(func)
    def check(request, *args, **kwargs):
        try:
            data = request.request.session.get('session_id', None)
        except (AttributeError,):
            data = request.session.get('session_id', None)

        if data:
            try:
                api = API(token=data, method="/client.get.info/")
                api()
                return func(request, *args, **kwargs)

            except (Exception,) as error:
                if request.request.path == "/clients/login/":
                    pass
                else:
                    return HttpResponseRedirect('/clients/login/')

        else:
            if request.request.path == "/clients/login/":
                pass
            else:
                return HttpResponseRedirect('/clients/login/')

    return check

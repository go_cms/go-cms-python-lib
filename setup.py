__author__ = 'degibenz'
from setuptools import setup, find_packages

setup(
    name="gocms_api",
    version="0.1",
    packages=['gocms'],
    install_requires=['requests', 'simplejson'],
    author="degibenz",
    author_email="alecseishkil@gmail.com",
    url="https://bitbucket.org/go_cms/go-cms-python-lib"
)
